# -*- coding: utf:8 -*-
from website.models import Contato, Indicacao
from django.forms.models import ModelForm

class ContatoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContatoForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['%s' % field].widget.attrs['class'] = 'input-xlarge'

    class Meta:
        model = Contato


class IndicacaoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(IndicacaoForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['%s' % field].widget.attrs['class'] = 'input-xlarge'

    class Meta:
        model = Indicacao
