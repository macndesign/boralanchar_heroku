# -*- coding: utf:8 -*-
from website.models import Contato, Indicacao
from django.contrib import admin
from django.contrib.admin.options import ModelAdmin


class ContatoAdmin(ModelAdmin):
    list_display = ('nome',)

admin.site.register(Contato, ContatoAdmin)


class IndicacaoAdmin(ModelAdmin):
    list_display = ('nome',)
    
admin.site.register(Indicacao, IndicacaoAdmin)
