# coding: utf-8
from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('website.views',
    url(r'^$', 'index', name='index'),
    url(r'^como-funciona/$', 'como_funciona', name='como_funciona'),
    url(r'^contato/$', 'contato', name='contato'),

    url(r'^meus-pedidos/$', 'meus_pedidos', name='meus_pedidos'),
    url(r'^cadastre-se/$', 'cadastre_se', name='cadastre_se'),
    url(r'^login/$', 'login', name='login'),
    url(r'^indicar-estabelecimento/$', 'indicar_estabelecimento', name='indicar_estabelecimento'),

    url(r'^como-acompanhar-meu-pedido/$', 'como_acompanhar_meu_pedido', name='como_acompanhar_meu_pedido'),
    url(r'^perguntas-frequentes/$', 'perguntas_frequentes', name='perguntas_frequentes'),
    url(r'^termos-de-uso/$', 'termos_de_uso', name='termos_de_uso'),
    url(r'^empresa/$', 'empresa', name='empresa'),

    url(r'^vantagens-estabelecimento/$', 'vantagens_estabelecimento', name='vantagens_estabelecimento'),
    url(r'^area-estabelecimento/$', 'area_estabelecimento', name='area_estabelecimento'),
)
