# coding: utf-8
from django.contrib import messages
from website.forms import ContatoForm, IndicacaoForm
from django.shortcuts import render_to_response, render
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_protect


def index(request):
    return render(request, 'index.html')


@csrf_protect
def contato(request):
    if request.method == 'POST':
        form = ContatoForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, u'Contato enviado com sucesso!')
            form = ContatoForm()
        else:
            messages.add_message(request, messages.ERROR, u'Erro no envio!')
    else:
        form = ContatoForm()
        
    return render_to_response('contato.html', locals(), context_instance=RequestContext(request),)


def indicar_estabelecimento(request):
    if request.method == 'POST':
        form = IndicacaoForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, u'Estabelecimento enviado com sucesso!')
            form = IndicacaoForm()
        else:
            messages.add_message(request, messages.ERROR, u'Erro no envio!')

    else:
        form = IndicacaoForm()
        
    return render_to_response('indicar_estabelecimento.html', locals(), context_instance=RequestContext(request),)


def como_funciona(request):
    return render_to_response('como_funciona.html', {}, context_instance=RequestContext(request))


def meus_pedidos(request):
    return render_to_response('meus_pedidos.html', {}, context_instance=RequestContext(request))


def cadastre_se(request):
    return render_to_response('cadastre_se.html', {}, context_instance=RequestContext(request))


def login(request):
    return render_to_response('login.html', {}, context_instance=RequestContext(request))


def como_acompanhar_meu_pedido(request):
    return render_to_response('como_acompanhar_meu_pedido.html', {}, context_instance=RequestContext(request))


def perguntas_frequentes(request):
    return render_to_response('perguntas_frequentes.html', {}, context_instance=RequestContext(request))


def termos_de_uso(request):
    return render_to_response('termos_de_uso.html', {}, context_instance=RequestContext(request))


def empresa(request):
    return render_to_response('empresa.html', {}, context_instance=RequestContext(request))


def vantagens_estabelecimento(request):
    return render_to_response('vantagens_estabelecimento.html', {}, context_instance=RequestContext(request))


def area_estabelecimento(request):
    return render_to_response('area_estabelecimento.html', {}, context_instance=RequestContext(request))
