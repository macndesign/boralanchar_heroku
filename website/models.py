# coding: utf-8
from django.db import models


class Contato(models.Model):
    nome = models.CharField('Nome', max_length=255)
    twitter = models.CharField('Twitter @', blank=True, null=True, max_length=255)
    facebook = models.CharField('facebook.com/', blank=True, null=True, max_length=255)
    email = models.EmailField('Email', blank=True, null=True, max_length=255)
    mensagem = models.TextField('Mensagem', blank=True, null=True, max_length=1024)

    def __unicode__(self):
        return self.nome

    class Meta:
        ordering = ['nome']


class Indicacao(models.Model):
    nome = models.CharField('Nome', max_length=255)
    telefone = models.CharField('Telefone', blank=True, null=True, max_length=255)
    email = models.EmailField('Email', blank=True, null=True, max_length=255)
    endereco = models.CharField('Endereço', blank=True, null=True, max_length=255)
    twitter = models.CharField('Twitter @', blank=True, null=True, max_length=255)
    facebook = models.CharField('facebook.com/', blank=True, null=True, max_length=255)
    '''
    site = models.CharField(max_length=255, blank=True)
    '''
    descricao = models.TextField('Descrição', blank=True, null=True, max_length=1024)

    def __unicode__(self):
        return self.nome

    class Meta:
        ordering = ['nome']
        verbose_name = u'Indicação'
        verbose_name_plural = u'Indicações'
