from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from accounts.forms import UserAuthenticationForm
from accounts.views import register, activate, profile, update

urlpatterns = patterns('',
                       url(r'^activate/complete/$',
                           TemplateView.as_view(template_name="accounts/activation_complete.html"),
                           name='registration_activation_complete'),
                       # Activation keys get matched by \w+ instead of the more specific
                       # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
                       # that way it can return a sensible "invalid key" message instead of a confusing 404.
                       url(r'^activate/(?P<activation_key>\w+)/$', activate, name='registration_activate'),
                       url(r'^register/$', register, name='registration_register'),
                       url(r'^register/complete/$',
                           TemplateView.as_view(template_name="accounts/registration_complete.html"),
                           name='registration_complete'),

                       # Auth urls
                       url(r'^login/$', 'django.contrib.auth.views.login',
                           {'template_name': 'accounts/login.html',
                            'authentication_form': UserAuthenticationForm}, name='auth_login'),
                       url(r'^logout/$', 'django.contrib.auth.views.logout',
                           {'template_name': 'accounts/logout.html'}, name='auth_logout'),
                       url(r'^password/change/$', 'django.contrib.auth.views.password_change',
                           {'template_name': 'accounts/password_change_form.html'}, name='auth_password_change'),
                       url(r'^password/change/done/$', 'django.contrib.auth.views.password_change_done',
                           {'template_name': 'accounts/password_change_done.html'}, name='auth_password_change_done'),
                       url(r'^password/reset/$', 'django.contrib.auth.views.password_reset',
                           {'template_name': 'accounts/password_reset_form.html'}, name='auth_password_reset'),
                       url(r'^password/reset/done/$', 'django.contrib.auth.views.password_reset_done',
                           {'template_name': 'accounts/password_reset_done.html'}, name='auth_password_reset_done'),
                       url(r'^reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                           'django.contrib.auth.views.password_reset_confirm',
                           {'template_name': 'accounts/password_reset_confirm.html'},
                           name='auth_password_reset_confirm'),
                       url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete',
                           {'template_name': 'accounts/password_reset_complete.html'},
                           name='auth_password_reset_complete'),

                       # Profile
                       url(r'^profile/$', profile),
                       url(r'^register/update/(?P<pk>\d+)/$', update, name='registration_update'),
)