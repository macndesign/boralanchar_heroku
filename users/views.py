# coding: utf-8
from django.views.generic.base import TemplateView


class UserDashboardView(TemplateView):
    template_name = 'user_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(UserDashboardView, self).get_context_data(**kwargs)
        user = self.request.user
        context['estabelecimentos'] = user.estabelecimentos.all()
        return context
