# -*- coding:utf-8 -*-

from estabelecimento.models import Estabelecimento, Produto, STATUS_PEDIDO_CHOICES
from estabelecimento.models import STATUS_ESTABELECIMENTO_FECHADO, STATUS_ESTABELECIMENTO_ABERTO
from dashboard.forms import MudaStatusPedidoForm, FiltroStatusPedidoForm

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils import simplejson
from django.http import HttpResponse
from django.shortcuts import get_object_or_404 as get_object, render, redirect
from django.views.generic import TemplateView

from datetime import datetime, timedelta


class PedidosView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(PedidosView, self).get_context_data(**kwargs)
        request = self.request
        id_estabelecimento = kwargs['id_estabelecimento']
        estabelecimento = get_object(request.user.estabelecimentos, pk=id_estabelecimento)
        pedidos = estabelecimento.pedido_set.all()
        form = FiltroStatusPedidoForm(request.POST or request.GET or None)

        if form.is_bound and form.is_valid():
            filtro_tempo = form.cleaned_data['tempo']
            filtro_status = form.cleaned_data['status']

            timestamp = datetime.now() - timedelta(days=filtro_tempo)
            pedidos = pedidos.filter(data_hora__gt=timestamp)
            pedidos = pedidos.filter(status__in=filtro_status)

        # lista de listas (pedido, form)
        pedidos_plus_forms = [(pedido, MudaStatusPedidoForm(instance=pedido)) for pedido in pedidos]

        context['estabelecimento'] = estabelecimento
        context['form'] = form
        context['pedidos'] = pedidos_plus_forms
        return context

@login_required
def produtos_view(request, id_estabelecimento):
    estabelecimento = get_object(request.user.estabelecimentos, pk=id_estabelecimento)
    produto_set = estabelecimento.produto_set.all()
    return render(request, 'dashboard/produtos.html', {
        'estabelecimento': estabelecimento,
        'produtos': produto_set
    })


@login_required
def produto_toggle_disponibilidade(request, id_estabelecimento, id_produto):
    produto = get_object(Produto, pk=id_produto, estabelecimento=id_estabelecimento)

    produto.ativo = not produto.ativo
    produto.save()
    return redirect('dashboard:produtos', id_estabelecimento=id_estabelecimento)


@login_required
def pedidos_view(request, id_estabelecimento):
    estabelecimento = get_object(request.user.estabelecimentos, pk=id_estabelecimento)
    pedidos = estabelecimento.pedido_set.all()

    form = FiltroStatusPedidoForm(request.GET or None)

    if form.is_bound and form.is_valid():
        filtro_tempo = form.cleaned_data['tempo']
        filtro_status = form.cleaned_data['status']

        timestamp = datetime.now() - timedelta(days=filtro_tempo)
        pedidos = pedidos.filter(data_hora__gt=timestamp)
        pedidos = pedidos.filter(status__in=filtro_status)

    # lista de listas (pedido, form)
    pedidos = [(pedido, MudaStatusPedidoForm(instance=pedido)) for pedido in pedidos]

    return render(request, 'dashboard/pedidos.html', {
        'estabelecimento': estabelecimento,
        'pedidos': pedidos,
        'form_filtro':form,
        'lista_status_choices': STATUS_PEDIDO_CHOICES,
    })


@login_required
def muda_status_pedido(request, id_estabelecimento, id_pedido):
    estabelecimento = get_object(request.user.estabelecimentos, pk=id_estabelecimento)
    pedido = get_object(estabelecimento.pedido_set, pk=id_pedido)

    form = MudaStatusPedidoForm(request.POST or None, instance=pedido)
    status = 403

    if form.is_bound and form.is_valid():
        form.save()
        status = 200

    if request.is_ajax():
        response_data = simplejson.dumps({'id': "%06d" % pedido.id})
        return HttpResponse(response_data, status=status, mimetype='application/json')
    return redirect('dashboard_pedido', id_estabelecimento=id_estabelecimento, id_pedido=id_pedido)


@login_required
def pedido_detail(request, id_estabelecimento, id_pedido):
    estabelecimento = get_object(request.user.estabelecimentos, pk=id_estabelecimento)
    pedido = get_object(estabelecimento.pedido_set, pk=id_pedido)
    form = MudaStatusPedidoForm(request.POST or None, instance=pedido)
    act = request.GET.get('act')

    if form.is_bound:
        status = 403

        if form.is_valid():
            form.save()
            messages.info(request, message='Pedido %03d atualizado.' % estabelecimento.id)
            return redirect('dashboard_pedido', id_estabelecimento=id_estabelecimento, id_pedido=id_pedido)

    return render(request, 'dashboard/pedido.html', {
        'estabelecimento': estabelecimento,
        'pedido':pedido,
        'form': form,
        'act': act,
        'status_choices': STATUS_PEDIDO_CHOICES,
    })


def ajax_lista_pedidos(request, id_estabelecimento):
    if not request.user.is_authenticated():
        return HttpResponse('', status=404)

    estabelecimentos = request.user.estabelecimentos.all()
    estabelecimento = get_object(estabelecimentos, pk=id_estabelecimento)
    pedidos = estabelecimento.pedido_set.all()

    filters = request.GET.get('filters', None)

    if filters:
        st_list = [st for st in filters.split(',') if st.isdigit()]
        pedidos = pedidos.filter(status__in=st_list)

    return simplejson.dumps(pedidos)


@login_required
def toggle_estabelecimento(request, id_estabelecimento):
    """
    Muda o status do estabelecimento entre aberto e fechado
    """
    estabelecimentos = request.user.estabelecimentos.all()
    estabelecimento = get_object(estabelecimentos, pk=id_estabelecimento)

    if estabelecimento.status == STATUS_ESTABELECIMENTO_ABERTO:
        estabelecimento.status = STATUS_ESTABELECIMENTO_FECHADO
    else:
        estabelecimento.status = STATUS_ESTABELECIMENTO_ABERTO
    estabelecimento.save()
    return redirect('dashboard:pedidos', id_estabelecimento=id_estabelecimento)