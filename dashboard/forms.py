# -*- coding:utf-8 -*-
from estabelecimento.models import Pedido, STATUS_PEDIDO_CHOICES
from django import forms

TEMPO_CHOICES = (
    (1, '1 dia'),
    (2, '2 dias'),
    (5, '5 dias'),
    (10, '10 dias'),
    (30, '30 dias')
)

class FiltroStatusPedidoForm(forms.Form):
    status = forms.MultipleChoiceField(label='Filtro de status', required=False,
        initial=[x[0] for x in STATUS_PEDIDO_CHOICES],
        choices=STATUS_PEDIDO_CHOICES, widget=forms.CheckboxSelectMultiple)
    tempo = forms.TypedChoiceField(label='Mostrar pedidos até', required=True, coerce=int, choices=TEMPO_CHOICES)


class MudaStatusPedidoForm(forms.ModelForm):
    class Meta:
        model = Pedido
        fields = ['status']
