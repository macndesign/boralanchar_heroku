# -*- coding:utf-8 -*-

from django.conf.urls import patterns, url

urlpatterns = patterns('dashboard.views',
    url(r'^(?P<id_estabelecimento>\d+)/toggle/$',
        'toggle_estabelecimento', name='toggle_estabelecimento'),

    url(r'^(?P<id_estabelecimento>\d+)/pedidos/$',
        'pedidos_view', name='pedidos'),

    url(r'^(?P<id_estabelecimento>\d+)/pedido/(?P<id_pedido>\d+)/$',
        'pedido_detail', name='pedido'),

    url(r'^(?P<id_estabelecimento>\d+)/pedido/(?P<id_pedido>\d+)/edit/status/$',
        'muda_status_pedido', name='muda_status_pedido'),

    url(r'^(?P<id_estabelecimento>\d+)/produtos/$',
        'produtos_view', name='produtos'),

    url(r'^(?P<id_estabelecimento>\d+)/produto/(?P<id_produto>\d+)/toggle/$',
        'produto_toggle_disponibilidade', name='produto_toggle_disponibilidade'),
)