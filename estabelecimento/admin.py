# -*- coding: utf:8 -*-
from models import Estabelecimento, TipoCozinha, MeioPagamento
from models import CategoriaProduto, Produto, Cliente, Pedido, ItemPedido
from models import LocalEntrega, EtapaAdicional, Adicional
from models import Papel, Permissao, Colaborador, Visualizacao

from django.contrib import admin
from django.contrib.admin.options import ModelAdmin, TabularInline


#Dados de domínio
class TipoCozinhaAdmin(ModelAdmin):
    list_display = ('nome', 'descricao',)
    
admin.site.register(TipoCozinha, TipoCozinhaAdmin)


class MeioPagamentoAdmin(ModelAdmin):
    list_display = ('nome', 'descricao',)
    
admin.site.register(MeioPagamento, MeioPagamentoAdmin)


#Cliente
class ClienteAdmin(ModelAdmin):
    list_display = ('nome',)
    
admin.site.register(Cliente, ClienteAdmin)


#Produto
class AdicionalAdmin(ModelAdmin):
    list_display = ('produto', 'etapa_adicional',)
    
admin.site.register(Adicional, AdicionalAdmin)


class EtapaAdicionalInline (TabularInline):
    model = EtapaAdicional
    extra = 1
    fk_name = 'produto'


class ProdutoAdmin(ModelAdmin):
    list_display = ('nome', 'descricao', 'estabelecimento',)
    inlines = [EtapaAdicionalInline, ]
    
admin.site.register(Produto, ProdutoAdmin)


#Estabelecimento
class CategoriaProdutoAdmin (ModelAdmin):
    list_display = ('nome', 'descricao',)
    
admin.site.register(CategoriaProduto, CategoriaProdutoAdmin)


class LocalEntregaInline (TabularInline):
    model = LocalEntrega
    extra = 1


class EstabelecimentoAdmin(ModelAdmin):
    list_display = ('nome', 'descricao',)
    inlines = [LocalEntregaInline,]
    
admin.site.register(Estabelecimento, EstabelecimentoAdmin)


#Pedido
class ItemPedidoInline (TabularInline):
    model = ItemPedido
    extra = 1


class PedidoAdmin(ModelAdmin):
    list_display = ('estabelecimento', 'cliente',)
    inlines = [ItemPedidoInline, ]
    
admin.site.register(Pedido, PedidoAdmin)


class ColaboradorAdmin(ModelAdmin):
    list_display = ('papel', 'user', 'estabelecimento')


class VisualizacaoAdmin(ModelAdmin):
    list_display = ('detail',)

    def detail(self, obj):
        return obj.criado_em.strftime('%d/%m/%Y') + ' - ' + unicode(obj.estabelecimento)

admin.site.register(Papel)
admin.site.register(Permissao)
admin.site.register(Colaborador, ColaboradorAdmin)
admin.site.register(Visualizacao, VisualizacaoAdmin)