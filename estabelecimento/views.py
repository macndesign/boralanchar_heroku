# coding=utf-8
from django.conf import settings
from estabelecimento.models import Estabelecimento, TipoCozinha, MeioPagamento, \
    Produto, CategoriaProduto, Visualizacao
from estabelecimento.models import STATUS_ESTABELECIMENTO_FECHADO, \
    STATUS_ESTABELECIMENTO_ABERTO
from estabelecimento.templatetags.filtertag import FilterTag
from estabelecimento.utils import page
from location.forms import EnderecoForm

from cart import Cart
from cart.views import get_delivery_tax_from_request

from django.core.paginator import Paginator
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext

from datetime import datetime

def estabelecimentos(request):
    filter_tag = FilterTag(request)
    
    if not 'endereco' in request.session:
        estabelecimentos = []
        
        paginator = Paginator(estabelecimentos, settings.PAGINATOR_ITENS_PER_PAGE)
        
        filter_tag.context.update({
            'endereco_form': EnderecoForm(),
            'endereco': None,
            'estabelecimentos': estabelecimentos,
            'paginator': paginator,
            'page': page(request, paginator),
            'object_list': page(request, paginator).object_list,
            'status_estabelecimento_fechado': None,
            'status_estabelecimento_aberto': None,
            'tipos_de_cozinha': None,
            'meios_pagamentos': None,
        })
        
        return render_to_response('lista_estabelecimentos.html',
                                  filter_tag.context,
                                  context_instance=RequestContext(request))
    
    endereco = request.session['endereco']
    bairro = endereco.bairro
    
    locais_entrega = bairro.localentrega_set.all()
    estabelecimentos = Estabelecimento.objects.filter(pk__in=[i.estabelecimento.pk for i in locais_entrega])\
        .order_by('nome')

    if filter_tag.has_id_in_url('meio_pagamento'):
        meios_pagamentos_ids = filter_tag.get_list_id_from_url('meio_pagamento')
        estabelecimentos = estabelecimentos.filter(meios_pagamentos__in=meios_pagamentos_ids).distinct()

    if filter_tag.has_id_in_url('tipo_cozinha'):
        tipo_cozinha_ids = filter_tag.get_list_id_from_url('tipo_cozinha')
        estabelecimentos = estabelecimentos.filter(tipos_cozinha__in=tipo_cozinha_ids).distinct()

    if filter_tag.has_id_in_url('status'):
        status_ids = filter_tag.get_list_id_from_url('status')
        estabelecimentos = estabelecimentos.filter(status__in=status_ids).distinct()

    status_estabelecimento_fechado = estabelecimentos.filter(status__exact=STATUS_ESTABELECIMENTO_FECHADO)
    status_estabelecimento_aberto = estabelecimentos.filter(status__exact=STATUS_ESTABELECIMENTO_ABERTO)

    tipos_de_cozinha = TipoCozinha.objects.filter(estabelecimento__in=estabelecimentos).distinct().order_by('nome')
    
    meios_pagamentos = MeioPagamento.objects.filter(estabelecimento__in=estabelecimentos).distinct().order_by('nome')

    paginator = Paginator(estabelecimentos, settings.PAGINATOR_ITENS_PER_PAGE)

    filter_tag.context.update({
        'estabelecimentos': estabelecimentos,
        'object_list': page(request, paginator).object_list,
        'paginator': paginator,
        'page': page(request, paginator),
        'status_estabelecimento_fechado': status_estabelecimento_fechado,
        'status_estabelecimento_aberto': status_estabelecimento_aberto,
        'tipos_de_cozinha': tipos_de_cozinha,
        'meios_pagamentos': meios_pagamentos,
        'endereco_form': request.session['endereco'].get_form(),
        'endereco': endereco,
    })

    return render_to_response(
        'lista_estabelecimentos.html',
        filter_tag.context,
        context_instance=RequestContext(request)
    )


def estabelecimento_detail(request, pk):
    filter_tag = FilterTag(request)

    estabelecimento = get_object_or_404(Estabelecimento, pk=pk)
    produtos = Produto.objects.filter(estabelecimento=estabelecimento).order_by('categoria_produto')
    
    if filter_tag.has_id_in_url('categoria_produto'):
        categorias_na_url = filter_tag.get_list_id_from_url('categoria_produto')
        produtos = produtos.filter(categoria_produto__in=categorias_na_url)
    
    produtos_estabelecimento = Produto.objects.filter(estabelecimento=estabelecimento)
    categorias_produtos = CategoriaProduto.objects.filter(produto__in=produtos_estabelecimento).distinct()

    cart = Cart(request)
    delivery_tax = get_delivery_tax_from_request(request, cart)

    paginator = Paginator(produtos, settings.PAGINATOR_ITENS_PER_PAGE)

    filter_tag.context.update({
        'produtos_estabelecimento': produtos_estabelecimento,
        'estabelecimento': estabelecimento,
        'object_list': page(request, paginator).object_list,
        'categorias_produtos': categorias_produtos,
        'produtos': produtos,
        'taxa_de_entrega': delivery_tax,
        'valor_total': delivery_tax + cart.summary(),
        'cart': cart,
        'paginator': paginator,
        'page': page(request, paginator),
        'endereco': 'endereco' in request.session and request.session['endereco'] or None,
        'endereco_form': 'endereco' in request.session and request.session['endereco'].get_form() or EnderecoForm(),
    })

    response = render_to_response(
        'estabelecimento_detail.html',
        filter_tag.context,
        context_instance=RequestContext(request),
    )

    cookie_key = 'boralanchar.visit.est.%d.%s' % (
        estabelecimento.id,
        datetime.today().strftime("%d%m%y")
    )

    cookie_value = request.COOKIES.get(cookie_key, None)

    if cookie_value != 'true':
        ip_address = request.META['REMOTE_ADDR']
        Visualizacao.objects.get_or_create(
            estabelecimento=estabelecimento,
            ip_address=ip_address)
        response.set_cookie(cookie_key, 'true')

    return response
