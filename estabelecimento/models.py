# -*- coding:utf-8 -*-
from location.models import Uf, Cidade, Bairro
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from datetime import datetime

STATUS_ESTABELECIMENTO_FECHADO = '0'
STATUS_ESTABELECIMENTO_ABERTO = '1'
STATUS_ESTABELECIMENTO_CHOICES = (
    (STATUS_ESTABELECIMENTO_FECHADO, _('Fechado')),
    (STATUS_ESTABELECIMENTO_ABERTO, _('Aberto')),
)


class Permissao(models.Model):
    nome = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)

    class Meta:
        verbose_name = 'permissão'
        verbose_name_plural = 'permissões'

    def __unicode__(self):
        return self.nome


class Papel(models.Model):
    titulo = models.CharField('Título', max_length=50)
    descricao = models.TextField('Descrição', blank=True, null=True)
    permissoes = models.ManyToManyField(Permissao, related_name='papeis', verbose_name='Permissões', blank=True)

    class Meta:
        verbose_name = 'papel'
        verbose_name_plural = 'papeis'

    def __unicode__(self):
        return self.titulo


class Colaborador(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    estabelecimento = models.ForeignKey('Estabelecimento')
    papel = models.ForeignKey(Papel, related_name='colaboradores')

    class Meta:
        verbose_name = 'Colaborador'
        verbose_name_plural = 'Colaboradores'


class Visualizacao(models.Model):
    criado_em = models.DateTimeField(auto_now_add=True)
    estabelecimento = models.ForeignKey('Estabelecimento', related_name='visualizacoes')
    ip_address = models.IPAddressField()

    class Meta:
        verbose_name = 'Visualização'
        verbose_name_plural = 'Visualizações'


class Estabelecimento(models.Model):
    nome = models.CharField(max_length=50, blank=False)
    descricao = models.CharField(max_length=255, blank=True, verbose_name='Descrição')
    imagem = models.ImageField(upload_to=settings.UPLOAD_TO_IMAGES, blank=True)
    ativo = models.BooleanField(default=True)
    status = models.CharField(max_length=1, default=STATUS_ESTABELECIMENTO_FECHADO,
                              choices=STATUS_ESTABELECIMENTO_CHOICES)
    dataCadastro = models.DateTimeField(default=datetime.now(), verbose_name='Data de Cadastro')

    uf = models.ForeignKey(Uf)
    cidade = models.ForeignKey(Cidade)
    bairro = models.ForeignKey(Bairro)
    endereco = models.CharField(max_length=255, verbose_name='Endereço')
    numero = models.CharField(max_length=30, verbose_name='Número', blank=True)

    '''
    complemento = models.CharField(max_length=30, verbose_name='Complemento', blank=True)
    cep = models.CharField(max_length=8, verbose_name='CEP', blank=True)
    
    razao_social = models.CharField(max_length=255, blank=True, verbose_name='Descrição')
    email = models.EmailField('Email', blank = True, null = True, max_length = 255)
    cnpj = models.CharField(max_length=14, blank=True, verbose_name='CNPJ')
    
    telefone = models.CharField(max_length=11, blank=True, verbose_name='Telefone')
    celular = models.CharField(max_length=11, blank=True, verbose_name='Celular')
    
    site = models.CharField(max_length=255, blank=True)
    '''

    # membros com acesso parcial/total ao dashboard do estabelecimento
    colaboradores = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='estabelecimentos',
                                           through=Colaborador)

    meios_pagamentos = models.ManyToManyField('MeioPagamento', verbose_name='Meios de pagamento')
    tipos_cozinha = models.ManyToManyField('TipoCozinha', verbose_name='Tipos de cozinha')

    horario_funcionamento_domingo = models.CharField(max_length=20, verbose_name='Hor. Domingo')
    horario_funcionamento_segunda = models.CharField(max_length=20, verbose_name='Hor. Segunda')
    horario_funcionamento_terca = models.CharField(max_length=20, verbose_name='Hor. Terça')
    horario_funcionamento_quarta = models.CharField(max_length=20, verbose_name='Hor. Quarta')
    horario_funcionamento_quinta = models.CharField(max_length=20, verbose_name='Hor. Quinta')
    horario_funcionamento_sexta = models.CharField(max_length=20, verbose_name='Hor. Sexta')
    horario_funcionamento_sabado = models.CharField(max_length=20, verbose_name='Hor. Sábado')

    class Meta:
        ordering = ['nome']
        verbose_name = u'Estabelecimento'
        verbose_name_plural = u'Estabelecimentos'

    def __unicode__(self):
        return self.nome

    def save(self, *args, **kwargs):
        unicode(self.imagem.name)
        super(Estabelecimento, self).save(*args, **kwargs)

    def horario_funcionamento_hoje(self):
        return {
            0: self.horario_funcionamento_segunda,
            1: self.horario_funcionamento_terca,
            2: self.horario_funcionamento_quarta,
            3: self.horario_funcionamento_quinta,
            4: self.horario_funcionamento_sexta,
            5: self.horario_funcionamento_sabado,
            6: self.horario_funcionamento_domingo,
        }.get(datetime.today().weekday()) or None

    def funcionando(self):
        return self.status == STATUS_ESTABELECIMENTO_ABERTO

    #        hora = self.horario_funcionamento_hoje()
    #        m = re.search('(\d{1,2}:\d\d).+(\d{1,2}:\d\d)', hora)
    #
    #        try:
    #            if len(m.groups()) == 2:
    #                inicio = m.group(0)
    #                termino = m.group(1)
    #        except:
    #            pass
    #        # todo fix it
    #        return True

    def locais_entrega(self):
        return self.localentrega_set.all()

    def lst_meios_pagamentos(self):
        return self.meios_pagamentos.all()

    def lst_tipos_cozinha(self):
        return self.tipos_cozinha.all()


class LocalEntrega(models.Model):
    taxa_entrega = models.DecimalField(decimal_places=2, max_digits=9, verbose_name='Taxa de entrega')
    tempo_aproximado = models.CharField(max_length=20, verbose_name='Tempo aproximando')
    estabelecimento = models.ForeignKey(Estabelecimento)
    bairro = models.ForeignKey(Bairro)

    class Meta:
        verbose_name = u'Local de Entrega'
        verbose_name_plural = u'Locais de Entrega'

    def taxa_entrega_formatada(self):
        return ('%10.2f' % self.taxa_entrega).strip().replace('.', ',')


class TipoCozinha(models.Model):
    nome = models.CharField(max_length=50, blank=False)
    descricao = models.CharField(max_length=255, blank=True, verbose_name='Descrição')
    imagem = models.ImageField(upload_to=settings.UPLOAD_TO_IMAGES, blank=True)
    ativo = models.BooleanField(default=True)

    class Meta:
        ordering = ['nome']
        verbose_name = u'Tipo de cozinha'
        verbose_name_plural = u'Tipos de cozinha'

    def save(self, *args, **kwargs):
        unicode(self.imagem.name)
        super(TipoCozinha, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.nome


class MeioPagamento(models.Model):
    nome = models.CharField(max_length=50, blank=False)
    descricao = models.CharField(max_length=255, blank=True, verbose_name='Descrição')
    imagem = models.ImageField(upload_to=settings.UPLOAD_TO_IMAGES)
    ativo = models.BooleanField(default=True)

    class Meta:
        ordering = ['nome']
        verbose_name = u'Meio de pagamento'
        verbose_name_plural = u'Meios de pagamento'

    def save(self, *args, **kwargs):
        unicode(self.imagem.name)
        super(MeioPagamento, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.nome


class CategoriaProduto(models.Model):
    nome = models.CharField(max_length=50, blank=False)
    descricao = models.CharField(max_length=255, blank=True, verbose_name='Descrição')
    imagem = models.ImageField(upload_to=settings.UPLOAD_TO_IMAGES, blank=True)
    ativo = models.BooleanField(default=True)

    class Meta:
        ordering = ['nome']
        verbose_name = u'Categoria de produto'
        verbose_name_plural = u'Categorias de produto'

    def save(self, *args, **kwargs):
        unicode(self.imagem.name)
        super(CategoriaProduto, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.nome


class Produto(models.Model):
    nome = models.CharField(max_length=50, blank=False)
    descricao = models.CharField(max_length=255, blank=True, verbose_name='Descrição')
    imagem = models.ImageField(upload_to=settings.UPLOAD_TO_IMAGES, blank=True)
    ativo = models.BooleanField(default=True)
    preco = models.DecimalField(decimal_places=2, max_digits=9, verbose_name='Preço')
    somente_adicional = models.BooleanField(verbose_name='Este Produto é somente adicional?')
    nao_pode_ser_adicional = models.BooleanField(verbose_name='Este produto não deve ser um adicional?')
    estabelecimento = models.ForeignKey(Estabelecimento)
    categoria_produto = models.ForeignKey(CategoriaProduto, verbose_name='Categoria do produto')

    etapas_adicionais = models.ManyToOneRel('EtapaAdicional', field_name='etapas_adicionais')

    class Meta:
        ordering = ['nome']
        verbose_name = u'Produto'
        verbose_name_plural = u'Produtos'

    def __unicode__(self):
        return self.nome

    def save(self, *args, **kwargs):
        unicode(self.imagem.name)
        super(Produto, self).save(*args, **kwargs)

    def preco_formatado(self):
        return ('%10.2f' % self.preco).strip().replace('.', ',')


class EtapaAdicional(models.Model):
    nome = models.CharField(max_length=50, blank=False)
    descricao = models.CharField(max_length=255, blank=True, verbose_name='Descrição')
    imagem = models.ImageField(upload_to=settings.UPLOAD_TO_IMAGES, blank=True)
    ativo = models.BooleanField(default=True)
    ordem_exibicao = models.IntegerField(verbose_name='Ordem de exibição')
    quantidade_adicionais_simultaneos = models.IntegerField(
        verbose_name='Quantidade de adicionais que podem ser selecionados simultaneamente')
    exibir_opcao_nenhuma = models.BooleanField(verbose_name='Deve exibir a opção nenhuma?')
    cobrar_pelo_maior_preco = models.BooleanField(verbose_name='Devo cobrar pelo maior preço do adicional?')
    estabelecimento = models.ForeignKey(Estabelecimento)
    produto = models.ForeignKey(Produto, related_name='produto')
    adicional_padrao = models.ForeignKey(Produto, related_name='adicional_padrao', verbose_name='Adicional padrão')
    adicionais = models.ManyToOneRel('Adicional', field_name='adicionais')

    class Meta:
        ordering = ['nome']
        verbose_name = u'Etapa Adicional'
        verbose_name_plural = u'Etapas Adicionais'

    def __unicode__(self):
        return self.nome

    def save(self, *args, **kwargs):
        unicode(self.imagem.name)
        super(EtapaAdicional, self).save(*args, **kwargs)


class Adicional(models.Model):
    preco = models.DecimalField(decimal_places=2, max_digits=9, verbose_name='Preço')
    etapa_adicional = models.ForeignKey(EtapaAdicional)
    produto = models.ForeignKey(Produto)


class Cliente(models.Model):
    nome = models.CharField(max_length=50, blank=False)
    email = models.EmailField()
    ativo = models.BooleanField(default=True)
    data_nascimento = models.DateField(verbose_name='Data de nascimento')
    telefone = models.CharField(max_length=10)

    class Meta:
        ordering = ['nome']
        verbose_name = u'Cliente'
        verbose_name_plural = u'Clientes'

    def __unicode__(self):
        return self.nome


STATUS_PEDIDO_PENDENTE = '0'
STATUS_PEDIDO_RECEBIDO = '1'
STATUS_PEDIDO_EM_PREPARO = '2'
STATUS_PEDIDO_AGUARDANDO_ENTREGA = '3'
STATUS_PEDIDO_SAIU_PARA_ENTREGA = '4'
STATUS_PEDIDO_ENTREGUE = '5'
STATUS_PEDIDO_CANCELADO = '6'

STATUS_PEDIDO_CHOICES = (
    (STATUS_PEDIDO_PENDENTE, _('Pendente')),
    (STATUS_PEDIDO_RECEBIDO, _('Recebido')),
    (STATUS_PEDIDO_EM_PREPARO, _('Em Preparo')),
    (STATUS_PEDIDO_AGUARDANDO_ENTREGA, _('Aguardando Entrega')),
    (STATUS_PEDIDO_SAIU_PARA_ENTREGA, _('Saiu para Entrega')),
    (STATUS_PEDIDO_ENTREGUE, _('Entregue')),
    (STATUS_PEDIDO_CANCELADO, _('Cancelado')),
)


class Pedido(models.Model):
    troco_para = models.DecimalField(
        verbose_name='Troco solicitado para...', help_text='Valor a ser entregue pelo cliente',
        decimal_places=2, max_digits=9, default=0)
    # TODO: não integrado
    taxa_entrega = models.DecimalField(
        verbose_name='Taxa de entrega',
        decimal_places=2, max_digits=9, default=0)
    # TODO: não integrado
    valor_pedido = models.DecimalField(
        verbose_name='Valor dos itens pedidos', help_text='Não inclui taxas',
        decimal_places=2, max_digits=9, default=0)
    data_hora = models.DateTimeField()
    status = models.CharField(max_length=1, choices=STATUS_PEDIDO_CHOICES, blank=False, null=False)
    estabelecimento = models.ForeignKey(Estabelecimento)
    cliente = models.ForeignKey(Cliente)
    meio_pagamento = models.ForeignKey(MeioPagamento, verbose_name='Meio de pagamento')
    itens = models.ManyToOneRel('ItemPedido', field_name='itens')

    class Meta:
        ordering = ['estabelecimento']

    def __unicode__(self):
        return '%05d' % self.id

    def valor_total(self):
        if getattr(self, '__valor_total', None) is None:
            self.__valor_total = sum(
                map(lambda item: item.valor_total(),
                    list(self.itens.all()))
            )

        return self.__valor_total


class ItemPedido(models.Model):
    observacao = models.TextField(verbose_name=_('Instruções especiais'), blank=True, null=True)
    quantidade = models.PositiveIntegerField()
    valor = models.DecimalField(decimal_places=2, max_digits=9)
    pedido = models.ForeignKey(Pedido, related_name='itens')
    produto = models.ForeignKey(Produto)
    itens_adicionais = models.ManyToManyField('Produto', related_name='itens_adicionais', null=True, blank=True)

    class Meta:
        verbose_name = 'Item do pedido'
        verbose_name_plural = 'Itens do pedido'

    def __unicode__(self):
        return '%02dx %s' % (self.quantidade, self.produto)

    def valor_total(self):
        return self.quantidade * self.valor
