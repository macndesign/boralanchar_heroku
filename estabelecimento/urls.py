# coding: utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('estabelecimento.views',
                       url(r'^$', 'estabelecimentos', name='estabelecimentos'),
                       url(r'^(?P<pk>\d+)/$', 'estabelecimento_detail', name='estabelecimento_detail'))
