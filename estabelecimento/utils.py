# coding=utf-8
from django.core.paginator import EmptyPage, InvalidPage


def page(request, paginator):
    try:
        selectedPage = int(request.GET.get('p', '1'))
    except ValueError:
        selectedPage = 1

    try:
        return paginator.page(selectedPage)
    except (EmptyPage, InvalidPage):
        return paginator.page(paginator.num_pages)
