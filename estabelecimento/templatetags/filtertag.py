# -*- coding: utf:8 -*-
"""
Created on 11/06/2012
@author: ajrs
"""
from django import template

register = template.Library()


class FilterTag(object):
    def __init__(self, request, context={}):
        self.request = request
        self.full_path = request.get_full_path()
        self.context = context

        context.update({'filtertag': self}, )

    def get_list_id_from_url(self, propriedade):
        return self.request.GET.getlist(propriedade)

    def has_id_in_url(self, propriedade):
        return self.request.GET.get(propriedade) is not None


def get_full_path(context):
    return context['filtertag'].full_path


def do_filter_url(parser, token):
    try:
        format_string = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag é obrigatório um argumento simples" % token.contents.split()[0])

    return FilterUrlNode(format_string)


class FilterUrlNode(template.Node):
    def __init__(self, format_string):
        self.tagname = format_string[0]
        self.propriedade = format_string[1]

        try:
            self.id = format_string[2]
        except:
            self.id = ''

    def render(self, context):
        full_path = get_full_path(context)
        id = self.id

        if (id == ''):
            id = str(context[self.propriedade].id)

        propriedade_id = "%s=%s" % (self.propriedade, id)

        if (propriedade_id in full_path):
            url_acrescentada = ''
            index = full_path.find(propriedade_id)
            operador_propriedade = full_path[index - 1:index]
            path = full_path.replace(operador_propriedade + propriedade_id, '')

            if ('/&' in path):
                path = path.replace('/&', '/?')
        else:
            path = full_path
            operador = ('?' in full_path) and '&' or '?'
            url_acrescentada = "%s%s" % (operador, propriedade_id)

        return "%s%s" % (path, url_acrescentada)


register.tag("filter_url", do_filter_url)


def do_filter_mark(parser, token):
    try:
        format_string = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag é obrigatório informar a propriedade" % token.contents.split()[0])

    return FilterMarkNode(format_string)


class FilterMarkNode(template.Node):
    def __init__(self, format_string):
        self.propriedade = format_string[1]

        try:
            self.id = format_string[2]
        except:
            self.id = ''

        try:
            self.mark_string = format_string[3]
        except:
            self.mark_string = 'checked="checked"'

    def render(self, context):
        full_path = get_full_path(context)
        id = self.id

        if id == '' or id == "''":
            id = str(context[self.propriedade].id)

        propriedade_id = "%s=%s" % (self.propriedade, id)

        return (propriedade_id in full_path) and self.mark_string or ''


register.tag("filter_mark", do_filter_mark)
