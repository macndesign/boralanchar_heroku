from django import template

register = template.Library()


@register.filter(name='truncateletters')
def truncateletters(value, arg):
    """
    Truncates a string after a certain number of letters

    Argument: Number of letters to truncate after
    """

    def truncate_letters(s, num):
        """Truncates a string after a certain number of letters."""
        length = int(num)
        letters = [l for l in s]
        if len(letters) > length:
            letters = letters[:length]
            if not letters[-3:] == ['.', '.', '.']:
                letters += ['.', '.', '.']
        return ''.join(letters)

    try:
        length = int(arg)
    # invalid literal for int()
    except ValueError:
        # Fail silently
        return value
    if not isinstance(value, basestring):
        value = str(value)
    return truncate_letters(value, length)