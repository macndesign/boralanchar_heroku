# -*- coding: utf:8 -*-
"""
Created on 12/07/2012
@author: ajrs
"""
from django import template
from django.utils.translation import gettext_lazy as _
import re

register = template.Library()


class EvalNode(template.Node):
    def __init__(self, parcela_expressao, nome_variavel):
        self.parcela_expressao = parcela_expressao
        self.nome_variavel = nome_variavel
    
    def render(self, context):
        try:
            lista_contexto = list(context)
            lista_contexto.reverse()
            novo_contexto = {}
            novo_contexto['_'] = _

            for item_contexto in lista_contexto:
                novo_contexto.update(item_contexto)

            if self.nome_variavel:
                context[self.nome_variavel] = eval(self.parcela_expressao, novo_contexto)
                return ''
            else:
                return str(eval(self.parcela_expressao, novo_contexto))
        except:
            raise

sintaxe_expressao = re.compile(r'(.*?)\s+as\s+(\w+)', re.DOTALL)

def do_eval(parser, token):
    try:
        tag_name, expressao = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag é necessário inserir uma expressão" % token.contents[0]
    
    entrada_tag = sintaxe_expressao.search(expressao)
    
    if entrada_tag:
        parcela_expressao, nome_variavel = entrada_tag.groups()
    else:
        if not expressao:
            raise template.TemplateSyntaxError, "%r tag at least require one argument" % tag_name
            
        parcela_expressao, nome_variavel = expressao, None
    
    return EvalNode(parcela_expressao, nome_variavel)

do_eval = register.tag('eval', do_eval)
