# coding:utf-8

from django import template
import re

register = template.Library()

def __format_pint(old):
    new = re.sub("^(-?\d+)(\d{3})", '\g<1>.\g<2>', old)

    if old == new:
        return new
    else:
        return __format_pint(new)


def format_money(value):
    value = '%10.2f' % value
    pint, fraction = value.split('.')
    return ','.join((__format_pint(pint), fraction))

@register.filter(name='format_money')
def format_money_filter(value):
    return format_money(value)
