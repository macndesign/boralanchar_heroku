# -*- coding:utf-8 -*-
from estabelecimento.models import Produto as Product
from estabelecimento.models import LocalEntrega
from templatetags.cart import format_money
from cart import Cart
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from django.http import HttpResponse
import json


def get_delivery_tax_from_request(request, cart):
    try:
        address = request.session['endereco']
        delivery_tax = get_delivery_tax(cart.cart.item_set.all(), address)
    except IndexError:
        delivery_tax = 0
    return delivery_tax


def get_delivery_tax(item_set, address):
    u"""
    Informa a taxa de entrega para um endereço baseado no estabelecimento
    do primeiro produto registrado no item_set.

    Obs
    Caso haja produtos de diferentes estabelecimentos, este método não
    funciona.
    """
    try:
        item = item_set[0]
        product = item.product
        store = product.estabelecimento
        neighborhood = address.bairro
        delivery = LocalEntrega.objects.get(estabelecimento=store, bairro=neighborhood)
        return delivery.taxa_entrega
    except IndexError:
        return 0
    except LocalEntrega.DoesNotExist:
        return None


def cart_items_to_json(cart, delivery_tax=0):
    _items = []
    for item in cart:
        product = item.product
        _items.append({
            'id_item': item.id,
            'id_produto': product.id,
            'nome_produto': product.nome,
            'quantidade': item.quantity,
            'observacao': item.observation,
            'preco_unidade': format_money(item.unit_price),
            'preco_total': format_money(item.total_price)
        })

    data = {
        'items': _items,
        'total_value': str(delivery_tax + cart.summary()),
        'formatted_total_value': format_money(delivery_tax + cart.summary()),
        'delivery_tax': str(delivery_tax),
        'formatted_delivery_tax': format_money(delivery_tax),
        'summary': str(cart.summary()),
        'formatted_summary': format_money(cart.summary())
    }
    return json.dumps(data)


def add_to_cart(request, product_id, quantity, ensure_new=False):
    product = Product.objects.get(id=product_id)
    cart = Cart(request)

    if ensure_new:
        observation = request.POST.get('observation', None)
        cart.add_new(product, product.preco, quantity, observation)
    else:
        cart.add(product, product.preco, quantity)

    if request.is_ajax():
        delivery_tax = get_delivery_tax_from_request(request, cart)
        return HttpResponse(
            cart_items_to_json(cart, delivery_tax),
            mimetype='application/json',
            status=200)
    return redirect(reverse('cart:get'))


def remove_from_cart(request, product_id):
    product = Product.objects.get(id=product_id)
    cart = Cart(request)
    cart.remove(product)

    if request.is_ajax():
        delivery_tax = get_delivery_tax_from_request(request, cart)
        return HttpResponse(
            cart_items_to_json(cart, delivery_tax),
            mimetype='application/json',
            status=200)
    return redirect(reverse('cart:get'))


def decrement_item_quantity(request, item_id, quantity=1):
    cart = Cart(request)
    cart.decrement_item(item_id, quantity)

    if request.is_ajax():
        delivery_tax = get_delivery_tax_from_request(request, cart)
        return HttpResponse(
            cart_items_to_json(cart, delivery_tax),
            mimetype='application/json',
            status=200
        )
    return redirect(reverse('cart:get'))


def decrement_quantity(request, product_id, quantity):
    product = Product.objects.get(id=product_id)
    cart = Cart(request)
    cart.decrement(product, product.preco, quantity)

    if request.is_ajax():
        delivery_tax = get_delivery_tax_from_request(request, cart)
        return HttpResponse(
            cart_items_to_json(cart, delivery_tax),
            mimetype='application/json',
            status=200
        )
    return redirect(reverse('cart:get'))


def get_cart(request):
    cart = Cart(request)
    delivery_tax = get_delivery_tax_from_request(request, cart)

    if request.is_ajax():
        return HttpResponse(
            cart_items_to_json(cart, delivery_tax),
            mimetype='application/json',
            status=200)
    else:
        return render_to_response(
            'cart/cart.html',
            dict(
                cart=cart,
                taxa_entrega=delivery_tax,
                taxa_entrega_formatada=format_money(delivery_tax),
            ),
            context_instance = RequestContext(request),
        )