# coding: utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('cart.views',
    url(r'^$', 'get_cart', name='get'),
    url(r'^add/(?P<product_id>\d+)/(?P<quantity>\d+)/$', 'add_to_cart', name='add'),
    url(r'^add/(?P<product_id>\d+)/(?P<quantity>\d+)/new/$', 'add_to_cart',
        kwargs={'ensure_new': True}, name='add_new'),
    url(r'^decrement/(?P<product_id>\d+)/(?P<quantity>\d+)/$', 'decrement_quantity', name='decrement'),
    url(r'^decrement/item/(?P<item_id>\d+)/(?P<quantity>\d+)/$',
        'decrement_item_quantity', name='decrement_item'),
    url(r'^remove/(?P<product_id>\d+)/$', 'remove_from_cart', name='remove'),
)
