# coding: utf-8

from users.views import UserDashboardView
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),

                       url(r'^estabelecimento/', include('estabelecimento.urls', namespace='estabelecimento')),
                       url(r'^dashboard/', include('dashboard.urls', namespace='dashboard')),
                       url(r'^location/', include('location.urls', namespace='location')),

                       # User dashboard
                       url(r'^user/dashboard/$', UserDashboardView.as_view(), name='user_dashboard'),

                       # Accounts
                       url(r'^accounts/', include('accounts.urls')),

                       # Cart
                       url(r'^cart/', include('cart.urls', namespace='cart')),

                       url(r'^', include('website.urls', namespace='website'))) + static(settings.MEDIA_URL,
                                                                                              document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
