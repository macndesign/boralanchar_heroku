var $option_vazio = $("<option value='0'></option>");
var $option_carregado = $("<option value='0'>-- Selecione --</option>");

function preenche(combo, values){
	var comboObj = $(combo);
	
	if(values.length > 0){
		comboObj.removeAttr("disabled");
		comboObj.html($option_carregado);
    } else {
    	comboObj.html($option_vazio);
    }
	
    $.each(values, function(i, item) {
    	comboObj.append(
                "<option id='" + values[i].pk + "' value='" + values[i].pk + "'>"
                + values[i].nome
                + "</option>"
        );
    });
}