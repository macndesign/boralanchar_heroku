(function($) {
	$(document).ready(function() {
		boralanchar = new Object();
		boralanchar.Geolocation = function(urlGet) {

			var result = {
				urlGet: urlGet,
				noGeolocation: null,
				success: null,
				fail: null,
				loading: null,
				
				uf: '#uf',
				cidade: '#cidade',
				bairro: '#bairro',

				start: function() {
					if(this.loading)
						this.loading(this);
					
					var success = this.success;
					var urlGet = this.urlGet;
						
					if (navigator && navigator.geolocation) {
						var locationFunction = function(position){
							doLocation(position, urlGet, success);
						};
						
						var errorCallback = function(error){
							doFail(error);
						};
						
						navigator.geolocation.getCurrentPosition(locationFunction,
																 errorCallback,
																 { maximumAge:Infinity, timeout:5000 });
				   } else {
					   console.log("O Geolocation não é suportado.");
					   
				       if(this.noGeolocation)
				    	   this.noGeolocation(this);
				   }
				}
			};
			
			function doSuccess(data){
				$(result.uf).val(data['uf'].pk);
				
				preenche(result.cidade, data['uf'].cidades);
				$(result.cidade).val(data['cidade'].pk);
				
				preenche(result.bairro, data['cidade'].bairros);
				$(result.bairro).val(data['bairro'].pk);
				
				if(result.success)
					result.success(result, data);
			}
			
			function doFail(status){
				//error.code == 
				//0: unknown error, 1: permission denied,
			    //2: position unavailable (error response from locaton provider), 3: timed out
				console.log('boralanchar.Geolocation.error: ' + status);
				
				if(result.fail)
					result.fail(result, status);
			}
			
			function doLocation(position, urlGet, success) {
			    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

			    var geocoder = new google.maps.Geocoder();

			    geocoder.geocode({'latLng': latlng}, function(results, status) {
			        if (status == google.maps.GeocoderStatus.OK) {

			            var uf = results[0]['address_components'][4]['short_name'];
			            var cidade = results[0]['address_components'][3]['long_name'];
			            var bairro = results[0]['address_components'][2]['long_name'];

			        	$.get(urlGet, {u: uf, c: cidade, b: bairro}, doSuccess, "json").error(doFail);
			        } else {
			        	doFail(status);
			        }
			    });
			}
			
			return result;
		};
	});
})(jQuery);
